### Spike Driver
---
This is an addon for Deathstrider that adds a .500 S&W lever action rifle. It has a chance to spawn on chaingun spawns i.e. the same spawns as the Ripper, Venom and Hammerhead.

### Credits and Permissions
---
All the cool people who inspired or helped me make this mod:
- [Big Horn Armory](https://www.bighornarmory.com/) for creating the real life Model 89 "Spike Driver" which this is based on.
- Accensus, coding and gauntlet sprites
- Icarus, all of the weapon sprites. (Yes, I'm crediting myself. Yes, I have a massive ego.)

All the assets used in this mod are original or used with permission from the original creator, if you want to use any of the assets in this mod in your own content you must get permission from the orginal creator.